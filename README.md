# Keizaal

A Skyrim Minecraft plugin

## Features
- All [shouts](https://elderscrolls.fandom.com/wiki/Dragon_Shouts#Shouts) from Skyrim, Skyrim: Dawnguard, and Skyrim: Dragonborn
- Unique Minecraft-oriented effects that simulate their original counterpart in Skyrim
- Shout sounds and particle effects
- Cutomizable shout words, descriptions, and cooldowns
- Compass cooldown bar à la Skyrim's compass
- Words walls to learn shouts
- Dragon souls to spend to learn shouts
- All features can be enabled/disabled

### Planned features
- Complete customization of shouts
    - Change effect size, particles, sound, potion effects, and more.
- Custom shouts
    - Add completely new and custom shouts that you make yourself

## Installation
1. Download the latest build or clone the repository and build it yourself
2. Place the `Keizaal-x.x.x.jar` file in your server's `plugins/` folder
3. Start/restart/reload your server to generate the `Keizaal` plugin folder and configuration files
4. FUS RO DAH!
5. *(optional) customize the [configuration](#configuration) to your liking*

## Configuration

Default configuration file can be found [here](https://gitlab.com/ASTRELION/keizaal/-/blob/master/src/main/resources/config.yml). Configuration values are explained via comments in the file.

# Issues

If you encounter any problems, please open an issue [here](https://gitlab.com/ASTRELION/keizaal/-/issues/new).

# Author
Me!

<a href='https://ko-fi.com/O5O7XRIS' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi1.png?v=2' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
