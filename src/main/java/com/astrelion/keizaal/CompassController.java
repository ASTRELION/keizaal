package com.astrelion.keizaal;

import com.astrelion.keizaal.shouts.ShoutController;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Map;

public class CompassController implements Listener
{
    private final Keizaal plugin;

    // N NE E SE S SW W NW
    private final String cardinalColor = ChatColor.WHITE.toString() + ChatColor.BOLD.toString();
    // |||||
    private final String emptyColor = ChatColor.DARK_GRAY.toString();
    private final String endCapColor = ChatColor.GRAY.toString();

    private final String empty = "||||||||||||||||||||||||||||||||||||||||";
    private final String compassString =
        "S" + empty +
        "SW" + empty +
        "W" + empty +
        "NW" + empty +
        "N" + empty +
        "NE" + empty +
        "E" + empty +
        "SE" + empty;
    private final String endCapLeft = endCapColor + "<<| ";
    private final String endCapRight = endCapColor + " |>>";

    public CompassController(Keizaal plugin)
    {
        this.plugin = plugin;

        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent event)
    {
        Player player = event.getPlayer();

        ShoutController shoutController = getPlugin().getShoutController();
        Map<Player, BossBar> cooldownBarMap = shoutController.getCooldownBarMap();

        if (cooldownBarMap.containsKey(player))
        {
            BossBar bar = cooldownBarMap.get(event.getPlayer());
            updateCompassString(player, bar);
        }
    }

    public void updateCompassString(Player player, BossBar bar)
    {
        double yaw = player.getLocation().getYaw();

        if (Math.abs(yaw) >= 360)
        {
            yaw = yaw % 360;
        }

        if (yaw < 0)
        {
            yaw = 360 - Math.abs(yaw);
        }

        int i = (int) Math.round((double)compassString.length() * (Math.abs(yaw) / 360.0));
        String string = getCompassString(i);
        string = string.replace("N", cardinalColor + "N");
        string = string.replace("S", cardinalColor + "S");
        string = string.replace("E", cardinalColor + "E");
        string = string.replace("W", cardinalColor + "W");
        string = string.replace("|", emptyColor + "|");

        bar.setTitle(String.format("%s%s%s", endCapLeft, string, endCapRight));
    }

    public String getCompassString(int i)
    {
        StringBuilder builder = new StringBuilder();

        int low = i - 35;
        int high = i + 35;

        if (low < 0)
        {
            builder.append(this.compassString.substring(this.compassString.length() - Math.abs(low)));
            low = 0;
        }

        builder.append(this.compassString.substring(low, Math.min(high, this.compassString.length())));

        if (high > this.compassString.length())
        {
            builder.append(this.compassString.substring(0, high - this.compassString.length()));
        }

        return builder.toString();
    }

    public Keizaal getPlugin()
    {
        return this.plugin;
    }
}
