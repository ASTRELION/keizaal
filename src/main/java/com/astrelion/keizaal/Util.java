package com.astrelion.keizaal;

public class Util
{
    /**
     * Number of minutes in a Minecraft day.
     */
    public static final int MINUTES_PER_DAY = 20;

    /**
     * Convert the given amount of seconds to ticks.
     * @param seconds The amount of seconds
     * @return seconds * 20
     */
    public static int secondsToTicks(double seconds)
    {
        return (int) (seconds * 20);
    }

    /**
     * Convert the given amount of minutes to ticks.
     * @param minutes The amount of minutes
     * @return minutes * 60 * 20
     */
    public static int minutesToTicks(double minutes)
    {
        return secondsToTicks(minutes * 60);
    }
}
