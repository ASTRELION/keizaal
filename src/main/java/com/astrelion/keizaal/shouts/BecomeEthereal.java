package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

public class BecomeEthereal extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public BecomeEthereal(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ITEM_ELYTRA_FLYING;

        this.particle = Particle.SOUL_FIRE_FLAME;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        int duration = Util.secondsToTicks(3 + (5 * wordCount));

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        player.addPotionEffects(Arrays.asList(
            // brief glow
            new PotionEffect(PotionEffectType.GLOWING, Util.secondsToTicks(0.5), 0),
            // take no damage
            new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, duration, 4),
            // deal no damage
            new PotionEffect(PotionEffectType.WEAKNESS, duration, 4),
            // invisible
            new PotionEffect(PotionEffectType.INVISIBILITY, duration, 0)
        ));
    }
}
