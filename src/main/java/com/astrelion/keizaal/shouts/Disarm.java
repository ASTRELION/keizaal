package com.astrelion.keizaal.shouts;

import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;

public class Disarm extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public Disarm(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.UI_STONECUTTER_TAKE_RESULT;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.TEAL, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        World world = player.getWorld();
        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> nearby = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                world.playSound(entity.getLocation(), Sound.BLOCK_ANVIL_LAND, SoundCategory.PLAYERS, 1, 1);
                world.spawnParticle(Particle.SQUID_INK, entity.getLocation(), 5);

                if (entity instanceof Player)
                {
                    Player playerEntity = (Player) entity;
                    playerEntity.dropItem(true);
                }
                else
                {
                    LivingEntity livingEntity = (LivingEntity) entity;
                    Location entityLocation = livingEntity.getLocation();
                    ItemStack item = livingEntity.getEquipment().getItemInMainHand();

                    if (!item.getType().isEmpty())
                    {
                        livingEntity.getEquipment().setItemInMainHand(new ItemStack(Material.AIR));
                        world.dropItemNaturally(entityLocation.add(entityLocation.getDirection()), item);
                    }
                }
            }
        }
    }
}
