package com.astrelion.keizaal.shouts;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;

public class Cyclone extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public Cyclone(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ITEM_ELYTRA_FLYING;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.GRAY, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : entities)
        {
            if (ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                entity.setVelocity(new Vector(0, 0.5 * wordCount, 0));
            }
        }
    }
}
