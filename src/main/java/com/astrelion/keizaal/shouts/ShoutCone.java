package com.astrelion.keizaal.shouts;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;

/**
 * Represents a shout with an AOE cone
 */
public abstract class ShoutCone extends Shout
{
    /**
     * The height (length) of the cone of effect
     */
    protected double[] coneHeight = new double[3];
    /**
     * The radius of the cone's first base (nearest shouter)
     */
    protected double[] coneBase1 = new double[3];
    /**
     * The radius of the cone's second base (furthest shouter)
     */
    protected double[] coneBase2 = new double[3];

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name The name of the shout
     */
    public ShoutCone(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.coneHeight = new double[] {15, 25, 35};
        this.coneBase1 = new double[] {0, 0.5, 1};
        this.coneBase2 = new double[] {3, 5, 7};
    }

    /**
     * Test whether the given point is in the cone or not
     * @param point The point to check
     * @param location The origin (usually the player)
     * @param direction The direction of the cone
     * @param height The height (length) of the cone
     * @param base1 The closest base radius
     * @param base2 The furthest base radius
     * @return true if the point is contained by the described cone
     */
    protected static boolean isContained(Vector point, Vector location, Vector direction, double height, double base1, double base2)
    {
        // the points distance into the cone (closest point along the axis)
        double distanceIntoCone = point.clone().subtract(location).dot(direction);

        // reject if distance is outside of cone height
        if (distanceIntoCone < 0 || distanceIntoCone > height)
        {
            return false;
        }

        double coneRadius = base1 + ((distanceIntoCone / height) * base2);
        double orthDistance = point.clone().subtract(location).distance(direction.clone().multiply(distanceIntoCone));
        return orthDistance < coneRadius;
    }

    @Override
    protected void spawnParticles(
        Player player,
        int wordCount,
        Particle particle,
        int particleCount,
        Vector particleMaxOffset,
        double particleExtra,
        @Nullable Object particleData)
    {
        Location location = player.getLocation().add(new Vector(0, 1, 0));
        Vector direction = location.getDirection();
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        for (double h = 0; h < coneHeight; h++)
        {
            Vector pointOnLine = direction.clone().multiply(h);
            double coneRadius = coneBase1 + ((h / coneHeight) * coneBase2);
            Vector perpDirection = (new Vector(1, 1, 1)).divide(direction).normalize().multiply(-1);
            Vector pointOnSurface = pointOnLine.clone().add(perpDirection.clone().multiply(coneRadius));

            // triangle with a, b = current radius, and c = 1 -> ~1 particle per block around circumference
            double increment = Math.acos((Math.pow(coneRadius, 2) + Math.pow(coneRadius, 2) - Math.pow(1, 2)) / (2 * coneRadius * coneRadius));

            for (double angle = 0; angle < 2 * Math.PI; angle += increment)
            {
                Vector rotated = pointOnSurface.clone().rotateAroundAxis(direction, angle);

                player.spawnParticle(
                    particle,
                    location.clone().add(rotated),
                    particleCount,
                    particleMaxOffset.getX(), particleMaxOffset.getY(), particleMaxOffset.getZ(),
                    particleExtra,
                    particleData
                );
            }
        }
    }

    /**
     * Get cone height for the provided word count.
     * @param wordCount The amount of words used
     * @return The cone height
     */
    protected double getActualHeight(int wordCount)
    {
        return coneHeight[wordCount - 1];
    }

    /**
     * Get cone base1 for the provided word count.
     * @param wordCount The amount of words used
     * @return The base1 radius (closest to shouter)
     */
    protected double getActualBase1(int wordCount)
    {
        return coneBase1[wordCount - 1];
    }

    /**
     * Get cone base2 for the provided word count.
     * @param wordCount The amount of words used
     * @return The base2 radius (furthest from shouter)
     */
    protected double getActualBase2(int wordCount)
    {
        return coneBase2[wordCount - 1];
    }
}
