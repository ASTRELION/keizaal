package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.List;

public class AuraWhisper extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name The name of the shout
     */
    public AuraWhisper(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_BAT_TAKEOFF;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.RED, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof LivingEntity)
            {
                LivingEntity livingEntity = (LivingEntity) entity;
                livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 15 * wordCount, 1));
            }
        }
    }
}
