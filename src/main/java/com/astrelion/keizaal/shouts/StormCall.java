package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

public class StormCall extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public StormCall(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_LIGHTNING_BOLT_THUNDER;

        this.particle = Particle.SQUID_INK;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        World world = player.getWorld();

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        world.setThundering(true);
        world.setThunderDuration(Math.max(world.getThunderDuration(), Util.secondsToTicks(60 * wordCount)));

        LocalDateTime dateTime = LocalDateTime.now().plusSeconds(Util.secondsToTicks(world.getThunderDuration()));

        // strike every 3-6 seconds for duration
        taskRunner(player, wordCount, dateTime);
    }

    private void taskRunner(Player player, int wordCount, LocalDateTime dateTime)
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();
        Random random = new Random();

        scheduler.runTaskLater(this.shoutController.getPlugin(), () -> {

            lightningStrikes(player, wordCount, dateTime);

            if (LocalDateTime.now().isBefore(dateTime))
            {
                taskRunner(player, wordCount, dateTime);
            }

        }, Util.secondsToTicks(random.nextInt(3) + 4) * 20L);
    }

    /**
     * Strikes nearby entities randomly, then schedules next lightning in 3-6 seconds
     * @param player The player
     * @param wordCount The word count
     * @param dateTime The end DateTime
     */
    private void lightningStrikes(Player player, int wordCount, LocalDateTime dateTime)
    {
        World world = player.getWorld();
        List<Entity> nearbyEntities = player.getNearbyEntities(20 * wordCount, 20 * wordCount, 20 * wordCount);
        nearbyEntities.add(player); // shout can harm the player

        Random random = new Random();
        Entity entity = nearbyEntities.get(random.nextInt(nearbyEntities.size()));
        Location location = entity.getLocation();
        location.setX(location.getX() + (random.nextDouble() - 0.5) * 5);
        location.setZ(location.getZ() + (random.nextDouble() - 0.5) * 5);

        world.strikeLightning(location);

        this.taskRunner(player, wordCount, dateTime);
    }
}
