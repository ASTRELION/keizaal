package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.List;

public class SoulTear extends ShoutLine
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public SoulTear(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.maxDistance = new double[]{20, 40, 60};
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double distance = this.getActualDistance(wordCount);

        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();
        World world = player.getWorld();
        List<Block> lineOfSight = player.getLineOfSight(null, (int)Math.ceil(distance));

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        // Deal damage to the first entity along line of sight
        for (Block block : lineOfSight)
        {
            for (Entity entity : block.getLocation().getNearbyEntities(1, 1, 1))
            {
                if (entity instanceof LivingEntity && entity != player)
                {
                    LivingEntity livingEntity = (LivingEntity) entity;

                    // deal massive damage
                    double damage = livingEntity instanceof Player ? 6 * wordCount : 10 * wordCount;
                    livingEntity.damage(damage, player);

                    scheduler.runTaskLater(this.shoutController.getPlugin(), () ->
                    {
                        if (livingEntity.isDead())
                        {
                            Skeleton skeleton = (Skeleton)world.spawnEntity(livingEntity.getLocation(), EntityType.SKELETON);
                            skeleton.getEquipment().clear();

                            if (livingEntity instanceof Player)
                            {
                                skeleton.getEquipment().setHelmet(new ItemStack(Material.PLAYER_HEAD));
                            }
                            else if (livingEntity instanceof Creeper)
                            {
                                skeleton.getEquipment().setHelmet(new ItemStack(Material.CREEPER_HEAD));
                            }
                            else if (livingEntity instanceof WitherSkeleton)
                            {
                                skeleton.getEquipment().setHelmet(new ItemStack(Material.WITHER_SKELETON_SKULL));
                            }
                            else if (livingEntity instanceof Skeleton)
                            {
                                skeleton.getEquipment().setHelmet(new ItemStack(Material.SKELETON_SKULL));
                            }

                            List<Entity> nearby = skeleton.getNearbyEntities(32, 16, 32);

                            for (Entity e : nearby)
                            {
                                if (e instanceof Monster && e != player)
                                {
                                    skeleton.setTarget((LivingEntity) e);
                                    break;
                                }
                            }

                            this.spawnsController.addEntity(skeleton);
                        }
                    }, Util.secondsToTicks(2));

                    return;
                }
            }
        }
    }
}
