package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.List;

public class AnimalAllegiance extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name The name of the shout
     */
    public AnimalAllegiance(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_RAVAGER_ROAR;
        this.soundPitch = 0.75f;

        this.particle = Particle.HEART;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof Tameable)
            {
                Tameable animal = (Tameable) entity;

                // tame only untamed animals near player
                if (animal.getOwnerUniqueId() == null)
                {
                    animal.setOwner(player);

                    scheduler.runTaskLater(this.shoutController.getPlugin(), () ->
                    {
                        animal.setOwner(null);
                    }, Util.secondsToTicks(30 + (15 * (wordCount - 1))));
                }
            }
        }
    }
}
