package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;

public class DrainVitality extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public DrainVitality(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_WITCH_THROW;

        this.particle = Particle.SPELL_WITCH;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> nearby = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                LivingEntity livingEntity = (LivingEntity) entity;

                livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Util.secondsToTicks(30), wordCount >= 2 ? 2 : 1));

                if (wordCount >= 3)
                {
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.POISON, Util.secondsToTicks(30), 2));
                }
            }
        }
    }
}
