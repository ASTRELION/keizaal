package com.astrelion.keizaal.shouts;

import org.bukkit.entity.Player;

/**
 * Represents a Power. Powers can only be used once a day.
 */
public interface Power
{
    /**
     * Set the player on cooldown for the day
     * @param player The player to set on cooldown
     */
    void placeOnCooldown(Player player);

    /**
     * Lift the cooldown for the player
     * @param player The player
     */
    void liftCooldown(Player player);

    /**
     * Check if the given player is on cooldown
     * @param player The player to check
     * @return True if the player is on cooldown, false otherwise
     */
    boolean isOnCooldown(Player player);
}
