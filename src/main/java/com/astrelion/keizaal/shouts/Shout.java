package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.SpawnsController;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;

public abstract class Shout
{
    /** If this shout is enabled or not **/
    protected boolean enabled;

    /** The name of this shout **/
    protected final String name;
    /** Description of the shout **/
    protected String description;
    /** The 3 words of power for this shout **/
    protected String[] words = new String[3];
    /** The cooldowns for each respective word of power **/
    protected int[] cooldowns = new int[3];

    /** The sound to play upon shouting */
    protected Sound sound;
    /** The rough distance (radius) the shout should be heard from in blocks **/
    protected double soundDistance;
    /** The pitch of the sound; 0.5 = half speed, 2 = twice speed **/
    protected float soundPitch;
    /** The particle to spawn for this shout **/
    protected Particle particle;
    /** The amount of particles to spawn **/
    protected int particleCount;
    /** Maximum particle movement vector **/
    protected Vector particleMaxOffset;
    /** The 'extra' data for the particle (usually speed) **/
    protected double particleExtra;
    /** Particle data (only needed for Redstone color) **/
    protected Object particleData;

    protected ShoutController shoutController;
    protected SpawnsController spawnsController;

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     * @param shoutController The ShoutController
     * @param name The name of the shout (must match config name)
     */
    public Shout(ShoutController shoutController, String name)
    {
        this.shoutController = shoutController;
        this.spawnsController = shoutController.getPlugin().getSpawnsController();

        this.name = name;

        this.sound = Sound.ENTITY_GENERIC_EXPLODE;
        this.soundDistance = 16;
        this.soundPitch = 1;

        this.particle = Particle.SPELL_INSTANT;
        this.particleCount = 1;
        this.particleMaxOffset = new Vector(0, 0, 0);
        this.particleExtra = 0.01;
        this.particleData = null;
    }

    public String getName()
    {
        return this.name;
    }

    public String[] getWords()
    {
        return this.words;
    }

    public int[] getCooldowns()
    {
        return this.cooldowns;
    }

    /**
     * Cast this shout as given Player.
     * @param player The player that shouted
     * @param wordCount The number of shout words used
     */
    protected abstract void shout(Player player, int wordCount);

    protected void playSound(Player player, int wordCount)
    {
        World world = player.getWorld();
        Location location = player.getLocation();
        world.playSound(location, this.sound, SoundCategory.PLAYERS, this.getSoundVolume(), this.soundPitch);
    }

    /**
     * Spawn shout particles.
     * @param player The player to spawn particles for
     * @param wordCount The shout word count
     */
    protected void spawnParticles(Player player, int wordCount)
    {
        this.spawnParticles(
            player,
            wordCount,
            this.particle,
            this.particleCount,
            this.particleMaxOffset,
            this.particleExtra,
            this.particleData
        );
    }

    /**
     * Spawn shout particles.
     * @param player The player to spawn particles for
     * @param wordCount The shout word count
     * @param particle The Particle to spawn
     * @param particleCount The particle amount to spawn
     * @param particleExtra The particle 'extra' data (usually speed)
     */
    protected abstract void spawnParticles(
        Player player,
        int wordCount,
        Particle particle,
        int particleCount,
        Vector particleMaxOffset,
        double particleExtra,
        @Nullable Object particleData
    );

    protected void createShout()
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();
        scheduler.runTask(this.shoutController.getPlugin(), () -> {

        });
    }

    /**
     * Get the the volume of this shout based on soundDistance.
     * @return ~ soundDistance / 16. Rounded to nearest int
     */
    protected int getSoundVolume()
    {
        return (int)Math.ceil(this.soundDistance / 16.0);
    }

    public void loadConfiguration(
        boolean enabled,
        String description,
        String[] words,
        int[] cooldowns
    )
    {
        this.enabled = enabled;
        this.description = description;
        this.words = words;
        this.cooldowns = cooldowns;
    }
}
