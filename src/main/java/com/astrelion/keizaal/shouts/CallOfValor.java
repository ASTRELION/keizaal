package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.Vector;

public class CallOfValor extends ShoutBuff
{
    private String[] heroNames = {
        "Gormlaith Golden-Hilt",
        "Felldir the Old",
        "Hakon One-Eye"
    };

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public CallOfValor(ShoutController shoutController, String name)
    {
        super(shoutController, name);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();

        World world = player.getWorld();
        Location location = player.getTargetBlock(64).getLocation().add(new Vector(0, 1, 0));

        Wolf wolf = (Wolf) world.spawnEntity(location, EntityType.WOLF);
        wolf.setOwner(player);
        wolf.setCollarColor(DyeColor.WHITE);
        wolf.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(40);
        wolf.setHealth(40);

        wolf.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 1));

        wolf.setCustomName(this.heroNames[wordCount - 1]);
        wolf.setCustomNameVisible(true);

        this.spawnsController.addEntity(wolf);

        scheduler.runTaskLater(this.shoutController.getPlugin(), () ->
        {
            this.spawnsController.removeEntity(wolf);
        }, Util.secondsToTicks(60));
    }
}
