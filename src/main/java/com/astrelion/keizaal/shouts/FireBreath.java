package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

public class FireBreath extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public FireBreath(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.coneHeight = new double[] {20, 25, 35};
        this.coneBase1 = new double[] {1, 1.5, 2};
        this.coneBase2 = new double[] {3, 5, 7};

        this.sound = Sound.ENTITY_GHAST_SHOOT;

        this.particle = Particle.FLAME;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : entities)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                entity.setFireTicks(Util.secondsToTicks(2 * wordCount));
            }
        }
    }

    @Override
    protected void spawnParticles(
            Player player,
            int wordCount,
            Particle particle,
            int particleCount,
            Vector particleMaxOffset,
            double particleExtra,
            @Nullable Object particleData)
    {
        Random random = new Random();
        World world = player.getWorld();
        Location location = player.getLocation().add(new Vector(0, 1, 0));
        Vector direction = location.getDirection();
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        for (double h = 0; h < coneHeight; h++)
        {
            Vector pointOnLine = direction.clone().multiply(h);
            double coneRadius = coneBase1 + ((h / coneHeight) * coneBase2);
            Vector perpDirection = (new Vector(1, 1, 1)).divide(direction).normalize().multiply(-1);

            for (double r = 0; r < coneRadius; r += 1.0)
            {
                Vector pointOnSurface = pointOnLine.clone().add(perpDirection.clone().multiply(r));
                double increment = Math.acos((Math.pow(r, 2) + Math.pow(r, 2) - Math.pow(1, 2)) / (2 * r * r));

                for (double angle = 0; angle < 2 * Math.PI; angle += increment)
                {
                    Vector rotated = pointOnSurface.clone().rotateAroundAxis(direction, angle);

                    if (r + 1 >= coneRadius)
                    {
                        player.spawnParticle(
                            particle,
                            location.clone().add(rotated),
                            particleCount,
                            particleMaxOffset.getX(), particleMaxOffset.getY(), particleMaxOffset.getZ(),
                            particleExtra,
                            particleData
                        );
                    }

                    if (random.nextInt(10) == 0)
                    {
                        Block block = world.getBlockAt(location.clone().add(rotated));

                        if (block.isSolid())
                        {
                            Location temp = block.getLocation().clone();
                            temp.setY(block.getY() + 1);
                            Block tempBlock = temp.getBlock();

                            if (tempBlock.isEmpty())
                            {
                                tempBlock.setType(Material.FIRE);
                            }
                        }
                    }
                }
            }
        }
    }
}
