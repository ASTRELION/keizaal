package com.astrelion.keizaal.shouts;

import com.destroystokyo.paper.entity.Pathfinder;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;

import java.util.List;

public class ThrowVoice extends ShoutLine
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public ThrowVoice(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_VILLAGER_AMBIENT;
        this.soundDistance = 64;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        Location location = player.getTargetBlock(64).getLocation();
        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        this.playSound(player, wordCount, location);
        this.spawnParticles(player, wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof Player)
            {
                Player playerEntity = (Player) entity;
                playerEntity.sendMessage("You hear a voice in the air...");
            }
            else if (entity instanceof LivingEntity)
            {
                Mob livingEntity = (Mob) entity;
                Pathfinder pathfinder = livingEntity.getPathfinder();
                pathfinder.moveTo(location);
            }
        }
    }

    protected void playSound(Player player, int wordCount, Location location)
    {
        World world = player.getWorld();
        world.playSound(location, this.sound, SoundCategory.PLAYERS, this.getSoundVolume(), this.soundPitch);
    }
}
