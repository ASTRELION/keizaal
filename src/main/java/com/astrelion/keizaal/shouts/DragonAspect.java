package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

public class DragonAspect extends ShoutBuff implements Power
{
    private final Set<Player> onCooldown = new HashSet<>();

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public DragonAspect(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_ENDER_DRAGON_GROWL;

        this.particle = Particle.DRAGON_BREATH;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        if (!isOnCooldown(player))
        {
            placeOnCooldown(player);

            int duration = Util.secondsToTicks(60 * wordCount);

            this.playSound(player, wordCount);
            this.spawnParticles(player, wordCount);

            player.addPotionEffects(Arrays.asList(
                new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, duration, (6 * (wordCount / 3)) - 1),
                new PotionEffect(PotionEffectType.FIRE_RESISTANCE, duration, 0),
                new PotionEffect(PotionEffectType.SPEED, duration, 0),
                new PotionEffect(PotionEffectType.INCREASE_DAMAGE, duration, wordCount - 1)
            ));
        }
    }

    @Override
    public void placeOnCooldown(Player player)
    {
        this.onCooldown.add(player);

        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();
        scheduler.runTaskLaterAsynchronously(this.shoutController.getPlugin(), () ->
        {
            liftCooldown(player);
        }, Util.minutesToTicks(Util.MINUTES_PER_DAY));
    }

    @Override
    public void liftCooldown(Player player)
    {
        this.onCooldown.remove(player);
    }

    @Override
    public boolean isOnCooldown(Player player)
    {
        return this.onCooldown.contains(player);
    }
}
