package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class ClearSkies extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public ClearSkies(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_GENERIC_EXPLODE;

        this.particle = Particle.CAMPFIRE_COSY_SMOKE;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        World world = player.getWorld();

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        world.setClearWeatherDuration(Util.secondsToTicks(30 * wordCount));
    }
}
