package com.astrelion.keizaal.shouts;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;

/**
 * Represents a shout with a single-target line
 */
public abstract class ShoutLine extends Shout
{
    /** The maximum distance this shout can effect */
    protected double[] maxDistance = new double[3];
    /** How close the entity has to be from LOS **/
    protected Vector margin;

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public ShoutLine(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.margin = new Vector(1, 1, 1);

        this.particleCount = 5;
        this.particleExtra = 0.01;
    }

    @Override
    protected void spawnParticles(
        Player player,
        int wordCount,
        Particle particle,
        int particleCount,
        Vector particleMaxOffset,
        double particleExtra,
        @Nullable Object particleData)
    {
        Location location = player.getLocation();
        Vector direction = location.getDirection();
        double distance = this.getActualDistance(wordCount);

        for (double d = 0; d < distance; d += 1.0)
        {
            Location loc = location.clone().add(direction.clone().multiply(d));
            player.spawnParticle(
                particle,
                loc,
                particleCount,
                particleMaxOffset.getX(), particleMaxOffset.getY(), particleMaxOffset.getZ(),
                particleExtra,
                particleData
            );
        }
    }

    /**
     * Get the actual distance the shout covers from given words used
     * @param wordCount The amount of words used
     * @return The distance the shout should cover
     */
    protected double getActualDistance(int wordCount)
    {
        return this.maxDistance[wordCount - 1];
    }
}
