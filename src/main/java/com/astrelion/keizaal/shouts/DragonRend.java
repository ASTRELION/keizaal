package com.astrelion.keizaal.shouts;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.util.Vector;

import java.util.List;

public class DragonRend extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public DragonRend(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.coneHeight = new double[] {40, 55, 70};
        this.coneBase1 = new double[] {1, 1.5, 2};
        this.coneBase2 = new double[] {5, 10, 15};

        this.sound = Sound.ENTITY_EVOKER_CAST_SPELL;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(
            coneHeight,
            coneHeight,
            coneHeight
        );

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : entities)
        {
            if (entity.getType() == EntityType.ENDER_DRAGON &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                EnderDragon enderDragon = (EnderDragon) entity;
                enderDragon.setPhase(EnderDragon.Phase.LAND_ON_PORTAL);
            }
        }
    }
}
