package com.astrelion.keizaal.shouts;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.util.Vector;

import java.util.List;

public class BendWill extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public BendWill(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_GENERIC_EXPLODE;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.ORANGE, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> nearby = player.getNearbyEntities(coneHeight, coneHeight, coneBase1);

        for (Entity entity : nearby)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                if (entity instanceof Tameable)
                {
                    // tame animals
                    Tameable animal = (Tameable) entity;
                    animal.setOwner(player);
                }
                else if (entity instanceof Monster)
                {
                    // set monsters to target nearby entity
                    Monster monster = (Monster) entity;

                    for (Entity e : monster.getNearbyEntities(32, 32, 32))
                    {
                        if (e instanceof Monster && e != player)
                        {
                            monster.setTarget((LivingEntity) e);
                            break;
                        }
                    }
                }
            }
        }
    }
}
