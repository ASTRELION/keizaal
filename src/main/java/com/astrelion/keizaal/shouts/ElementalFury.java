package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

public class ElementalFury extends ShoutBuff
{

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public ElementalFury(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_RAVAGER_ROAR;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.RED, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        double baseAttackSpeed = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getBaseValue();
        player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(baseAttackSpeed * (1 + ((float)wordCount / 3)));

        scheduler.runTaskLater(this.shoutController.getPlugin(), () ->
        {
            player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(baseAttackSpeed);
        }, Util.secondsToTicks(60 * wordCount));
    }
}
