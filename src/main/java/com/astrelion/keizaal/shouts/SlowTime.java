package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.List;

public class SlowTime extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public SlowTime(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.BLOCK_SOUL_SAND_FALL;

        this.particle = Particle.SOUL;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        int duration = 4 + (4 * wordCount);
        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        player.addPotionEffects(Arrays.asList(
            new PotionEffect(PotionEffectType.SLOW, Util.secondsToTicks(duration), 1),
            new PotionEffect(PotionEffectType.SLOW_DIGGING, Util.secondsToTicks(duration), 1)
        ));

        for (Entity entity : nearby)
        {
            if (entity instanceof LivingEntity)
            {
                LivingEntity livingEntity = (LivingEntity) entity;
                livingEntity.addPotionEffects(Arrays.asList(
                    new PotionEffect(PotionEffectType.SLOW, Util.secondsToTicks(duration), wordCount + 1),
                    new PotionEffect(PotionEffectType.SLOW_DIGGING, Util.secondsToTicks(duration), 2 * wordCount)
                ));
            }
        }
    }
}
