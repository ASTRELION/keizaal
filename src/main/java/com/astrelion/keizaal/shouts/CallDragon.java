package com.astrelion.keizaal.shouts;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class CallDragon extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public CallDragon(ShoutController shoutController, String name)
    {
        super(shoutController, name);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        World world = player.getWorld();
        Location location = player.getTargetBlock(64).getLocation().add(new Vector(0, 1, 0));

        EnderDragon dragon = (EnderDragon) world.spawnEntity(location, EntityType.ENDER_DRAGON);
        dragon.setPhase(EnderDragon.Phase.HOVER);

        dragon.setCustomName("Odahviing");
        dragon.setCustomNameVisible(true);

        this.spawnsController.addEntity(dragon);
    }
}
