package com.astrelion.keizaal.shouts;

import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.List;

public class UnrelentingForce extends ShoutCone
{
    public UnrelentingForce(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.coneHeight = new double[] {15, 25, 35};
        this.coneBase1 = new double[] {1, 1.5, 2};
        this.coneBase2 = new double[] {3, 5, 7};

        this.sound = Sound.ENTITY_DRAGON_FIREBALL_EXPLODE;
        this.soundDistance = 32;

        this.particle = Particle.REDSTONE;
        this.particleCount = 3;
        this.particleExtra = 0.01;
        this.particleData = new Particle.DustOptions(Color.BLUE, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(
            coneHeight,
            coneHeight,
            coneHeight
        );

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : entities)
        {
            if (ShoutCone.isContained(
                entity.getLocation().toVector(),
                player.getLocation().toVector(),
                direction, coneHeight, coneBase1, coneBase2))
            {
                Vector pushDirection = entity.getLocation().toVector().subtract(location.toVector()).normalize();
                entity.setVelocity(pushDirection.multiply(wordCount).add(new Vector(0, wordCount / 3, 0)));
                entity.setFireTicks(0);
            }
        }
    }
}
