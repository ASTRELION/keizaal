package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.entity.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class KynesPeace extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public KynesPeace(ShoutController shoutController, String name)
    {
        super(shoutController, name);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof Breedable)
            {
                Breedable breedable = (Breedable) entity;
                breedable.setBreed(false);
                breedable.setTarget(null);
                breedable.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Util.secondsToTicks(60), wordCount));

                if (entity instanceof Wolf)
                {
                    Wolf wolf = (Wolf) entity;
                    wolf.setAngry(false);
                }
            }
        }
    }
}
