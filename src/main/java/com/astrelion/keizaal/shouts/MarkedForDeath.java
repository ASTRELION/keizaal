package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.List;

public class MarkedForDeath extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public MarkedForDeath(ShoutController shoutController, String name)
    {
        super(shoutController, name);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        for (Entity entity : entities)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                LivingEntity livingEntity = (LivingEntity) entity;
                livingEntity.addPotionEffects(Arrays.asList(
                    // increase damage taken
                    new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Util.secondsToTicks(60), -1 * wordCount),
                    new PotionEffect(PotionEffectType.POISON, Util.secondsToTicks(60), 1)
                ));
            }
        }
    }
}
