package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.List;

public class BattleFury extends ShoutBuff
{

    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name            The name of the shout
     */
    public BattleFury(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_RAVAGER_ROAR;

        this.particle = Particle.REDSTONE;
        this.particleData = new Particle.DustOptions(Color.RED, 1);
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        List<Entity> nearby = player.getNearbyEntities(16 * wordCount, 16 * wordCount, 16 * wordCount);

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        tempAttackSpeed(player, wordCount);

        for (Entity entity : nearby)
        {
            if (entity instanceof Tameable)
            {
                Tameable tameable = (Tameable) entity;

                if (tameable.getOwnerUniqueId().equals(player.getUniqueId()))
                {
                    tempAttackSpeed((LivingEntity) entity, wordCount);
                }
            }
        }
    }

    private void tempAttackSpeed(LivingEntity entity, int wordCount)
    {
        BukkitScheduler scheduler = this.shoutController.getPlugin().getServer().getScheduler();
        double baseAttackSpeed = entity.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getBaseValue();
        entity.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(baseAttackSpeed * (1 + ((float)wordCount / 3)));

        scheduler.runTaskLater(this.shoutController.getPlugin(), () ->
        {
            entity.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(baseAttackSpeed);
        }, Util.secondsToTicks(60 * wordCount));
    }
}
