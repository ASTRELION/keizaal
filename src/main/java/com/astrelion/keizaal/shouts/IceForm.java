package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.Util;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;

public class IceForm extends ShoutCone
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public IceForm(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.coneHeight = new double[] {20, 25, 35};
        this.coneBase1 = new double[] {1, 1.5, 2};
        this.coneBase2 = new double[] {3, 6, 9};

        this.sound = Sound.BLOCK_GLASS_BREAK;

        this.particle = Particle.SPELL_INSTANT;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        double coneHeight = this.getActualHeight(wordCount);
        double coneBase1 = this.getActualBase1(wordCount);
        double coneBase2 = this.getActualBase2(wordCount);

        Location location = player.getLocation();
        Vector direction = location.getDirection();

        List<Entity> entities = player.getNearbyEntities(coneHeight, coneHeight, coneHeight);

        this.playSound(player, wordCount);

        for (Entity entity : entities)
        {
            if (entity instanceof LivingEntity &&
                ShoutCone.isContained(
                    entity.getLocation().toVector(),
                    player.getLocation().toVector(),
                    direction, coneHeight, coneBase1, coneBase2))
            {
                LivingEntity livingEntity = (LivingEntity) entity;
                livingEntity.addPotionEffect(new PotionEffect(
                    PotionEffectType.SLOW, Util.secondsToTicks(10 * wordCount), 10
                ));

                // players mid-flight fall
                if (livingEntity instanceof Player)
                {
                    Player playerFall = (Player) livingEntity;
                    playerFall.setFlying(false);
                }
            }
        }
    }
}
