package com.astrelion.keizaal.shouts;

import com.astrelion.keizaal.CompassController;
import com.astrelion.keizaal.Keizaal;
import com.astrelion.keizaal.Util;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;

public class ShoutController implements Listener
{
    protected Map<String, Shout> shoutsMap = new HashMap<>();
    protected Map<Player, BossBar> cooldownBarMap = new HashMap<>();

    private final Keizaal plugin;

    private CompassController compassController;

    public ShoutController(Keizaal plugin)
    {
        this.plugin = plugin;

        Shout[] tempShoutList = {
            new AnimalAllegiance(this, "animal_allegiance"),
            new AuraWhisper(this, "aura_whisper"),
            new BattleFury(this, "battle_fury"),
            new BecomeEthereal(this, "become_ethereal"),
            new BendWill(this, "bend_will"),
            new CallDragon(this, "call_dragon"),
            new CallOfValor(this, "call_of_valor"),
            new ClearSkies(this, "clear_skies"),
            new Cyclone(this, "cyclone"),
            new Disarm(this, "disarm"),
            new Dismay(this, "dismay"),
            new DragonAspect(this, "dragon_aspect"),
            new DragonRend(this, "dragonrend"),
            new DrainVitality(this, "drain_vitality"),
            new ElementalFury(this, "elemental_fury"),
            new FireBreath(this, "fire_breath"),
            new FrostBreath(this, "frost_breath"),
            new IceForm(this, "ice_form"),
            new KynesPeace(this, "kynes_peace"),
            new MarkedForDeath(this, "marked_for_death"),
            new SlowTime(this, "slow_time"),
            new SoulTear(this, "soul_tear"),
            new StormCall(this, "storm_call"),
            new SummonDurnehviir(this, "summon_durnehviir"),
            new ThrowVoice(this, "throw_voice"),
            new UnrelentingForce(this, "unrelenting_force"),
            new WhirlwindSprint(this, "whirlwind_sprint")
        };

        for (Shout shout : tempShoutList)
        {
            shoutsMap.put(shout.name, shout);
        }
    }

    public Map<String, Shout> getShouts()
    {
        return this.shoutsMap;
    }

    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent asyncPlayerChatEvent)
    {
        BukkitScheduler scheduler = this.getPlugin().getServer().getScheduler();

        Player player = asyncPlayerChatEvent.getPlayer();
        String message = asyncPlayerChatEvent.getMessage();

        Map<String, Integer> playerShout = this.parseShout(message);

        this.getPlugin().getLogger().info(asyncPlayerChatEvent.getPlayer().getName() + " used " + playerShout);

        if (playerShout != null)
        {
            String shoutName = playerShout.keySet().stream().findFirst().get();
            Shout shout = this.shoutsMap.get(shoutName);

            if (!cooldownBarMap.containsKey(player))
            {
                BossBar cooldownBar = player.getServer().createBossBar("", BarColor.BLUE, BarStyle.SOLID);
                cooldownBar.addPlayer(player);
                cooldownBar.setProgress(0);
                cooldownBarMap.put(player, cooldownBar);
                compassController.updateCompassString(player, cooldownBar);
            }

            scheduler.runTask(this.getPlugin(), () ->
            {
                shout.shout(player, playerShout.get(shoutName));
            });

            if (this.cooldownBarMap.get(player).getProgress() == 0)
            {
                showCooldown(player, shout.getCooldowns()[playerShout.get(shoutName) - 1]);
            }
        }
    }

    private void showCooldown(Player player, double cooldown)
    {
        BossBar cooldownBar = this.cooldownBarMap.get(player);
        runCooldownTask(cooldownBar, 1.0 / cooldown, cooldown);
    }

    private void runCooldownTask(BossBar cooldownBar, double progress, double cooldown)
    {
        BukkitScheduler scheduler = this.getPlugin().getServer().getScheduler();
        scheduler.runTaskLater(this.getPlugin(), () ->
        {
            if (progress >= 1.0)
            {
                cooldownBar.setProgress(1.0);
                return;
            }

            cooldownBar.setProgress(progress);
            runCooldownTask(cooldownBar, progress + (1.0 / cooldown), cooldown);
        }, Util.secondsToTicks(1));
    }

    /**
     * Parse a shout from given string
     * @param shoutString The string to be parsed
     * @return The shout if found, null otherwise
     */
    public Map<String, Integer> parseShout(String shoutString)
    {
        String reducedShoutString = shoutString.toLowerCase().trim().replaceAll("/[^A-Za-z]/", "");
        String[] words = reducedShoutString.split(" ");

        for (Shout shout : shoutsMap.values())
        {
            int wordsMatched = 0;

            // only check first three words
            for (int i = 0; i < Math.min(words.length, 3); i++)
            {
                if (shout.getWords()[i].equalsIgnoreCase(words[i]))
                {
                    wordsMatched++;
                }
                else
                {
                    break;
                }
            }

            if (wordsMatched > 0)
            {
                final int finalWordsMatched = wordsMatched;
                return new HashMap<String, Integer>(){{
                    put(shout.getName(), finalWordsMatched);
                }};
            }
        }

        return null;
    }

    public Map<Player, BossBar> getCooldownBarMap()
    {
        return this.cooldownBarMap;
    }

    public Keizaal getPlugin()
    {
        return this.plugin;
    }

    public void register()
    {
        this.compassController = getPlugin().getCompassController();

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
}
