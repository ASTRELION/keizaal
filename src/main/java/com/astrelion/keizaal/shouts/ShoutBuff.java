package com.astrelion.keizaal.shouts;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import javax.annotation.Nullable;

public abstract class ShoutBuff extends Shout
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     * @param name The name of the shout
     */
    public ShoutBuff(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.particleCount = 50;
        this.particleExtra = 0.1;
        this.particleMaxOffset = new Vector(0.5, 0, 0.5);
    }

    @Override
    protected void spawnParticles(
        Player player,
        int wordCount,
        Particle particle,
        int particleCount,
        Vector particleMaxOffset,
        double particleExtra,
        @Nullable Object particleData)
    {
        World world = player.getWorld();
        Location location = player.getLocation();

        world.spawnParticle(
            particle,
            location,
            particleCount,
            particleMaxOffset.getX(), particleMaxOffset.getY(), particleMaxOffset.getZ(),
            particleExtra,
            particleData
        );
    }
}
