package com.astrelion.keizaal.shouts;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class WhirlwindSprint extends ShoutBuff
{
    /**
     * Initialize a new Shout. Shouts are composed of 3 words with 3 associated cooldowns.
     *
     * @param shoutController The ShoutController
     */
    public WhirlwindSprint(ShoutController shoutController, String name)
    {
        super(shoutController, name);

        this.sound = Sound.ENTITY_BAT_TAKEOFF;

        this.particle = Particle.EXPLOSION_NORMAL;
    }

    @Override
    protected void shout(Player player, int wordCount)
    {
        Location location = player.getLocation();
        Vector direction = location.getDirection();

        this.playSound(player, wordCount);
        this.spawnParticles(player, wordCount);

        player.setVelocity(direction.setY(0f).multiply(Math.pow(wordCount + 3f, 2f)).setY(0.25f));
    }
}
