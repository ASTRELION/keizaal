package com.astrelion.keizaal;

import com.astrelion.keizaal.shouts.Shout;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.yaml.snakeyaml.Yaml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ConfigurationController
{
    private final Keizaal plugin;

    public ConfigurationController(Keizaal plugin)
    {
        this.plugin = plugin;
    }

    public Keizaal getPlugin()
    {
        return this.plugin;
    }

    public void register()
    {
        FileConfiguration config = this.plugin.getConfig();
        //Map<String, Object> shouts = (Map<String, Object>) config.get("shouts");
        Map<String, Object> values = config.getValues(true);
        for (String key : values.keySet())
        {
            System.out.println(key);
        }

        ConfigurationSection shouts = config.getConfigurationSection("shouts");
        Map<String, Shout> shoutMap = this.getPlugin().getShoutController().getShouts();

        this.getPlugin().getLogger().info("Loading shout configurations...");
        for (String key : shoutMap.keySet())
        {
            Shout shout = shoutMap.get(key);
            ConfigurationSection shoutConfig = (ConfigurationSection) shouts.get(key);

            boolean shoutEnabled = shoutConfig.getBoolean("enabled");
            String shoutDescription = shoutConfig.getString("description");

            MemorySection wordSection = (MemorySection) shoutConfig.get("words");
            Map<String, Object> wordMap = wordSection.getValues(true);
            List<String> shoutWords = new ArrayList<>();
            List<Integer> shoutCooldowns = new ArrayList<>();

            for (String word : wordMap.keySet())
            {
                shoutWords.add(word);
                shoutCooldowns.add((Integer) wordMap.get(word));
            }

            shout.loadConfiguration(
                shoutEnabled,
                shoutDescription,
                shoutWords.toArray(new String[0]),
                shoutCooldowns.stream().mapToInt(Integer::intValue).toArray()
            );

            this.getPlugin().getLogger().info(String.format("Loaded shout %s. ", key));        }
    }
}
