package com.astrelion.keizaal;

import com.astrelion.keizaal.shouts.ShoutController;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;

/**
 * Keizaal (Skyrim) Minecraft plugin
 * @author ASTRELION
 */
public final class Keizaal extends JavaPlugin
{
    private ConfigurationController configurationController;
    private ShoutController shoutController;
    private CompassController compassController;
    private SpawnsController spawnsController;

    @Override
    public void onEnable()
    {
        this.configurationController = new ConfigurationController(this);
        this.spawnsController = new SpawnsController(this);
        this.shoutController = new ShoutController(this);
        this.compassController = new CompassController(this);

        this.configurationController.register();
        this.shoutController.register();

        this.getLogger().info("See https://gitlab.com/ASTRELION/keizaal for information.");
        this.getLogger().info("Successfully enabled.");
    }

    @Override
    public void onDisable()
    {
        Map<Player, BossBar> cooldownBarMap = this.shoutController.getCooldownBarMap();

        for (Player key : cooldownBarMap.keySet())
        {
            BossBar bar = cooldownBarMap.get(key);
            bar.removeAll();
        }

        this.spawnsController.clearEntities();

        this.getLogger().info("Successfully disabled.");
    }

    public ConfigurationController getConfigurationController()
    {
        return this.configurationController;
    }

    public ShoutController getShoutController()
    {
        return this.shoutController;
    }

    public CompassController getCompassController()
    {
        return this.compassController;
    }

    public SpawnsController getSpawnsController()
    {
        return this.spawnsController;
    }
}
