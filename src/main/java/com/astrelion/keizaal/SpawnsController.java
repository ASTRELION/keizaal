package com.astrelion.keizaal;

import org.bukkit.entity.Entity;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Responsible for managing entities spawned by shouts
 */
public class SpawnsController
{
    /**
     * Holds the UUID's of all entities spawned by this plugin
     */
    protected final Set<UUID> spawnedEntities;

    private final Keizaal plugin;

    public SpawnsController(Keizaal plugin)
    {
        this.plugin = plugin;
        this.spawnedEntities = new HashSet<>();
    }

    public Set<UUID> getSpawnedEntities()
    {
        return this.spawnedEntities;
    }

    public void addEntity(Entity entity)
    {
        this.spawnedEntities.add(entity.getUniqueId());
    }

    public void removeEntity(Entity entity)
    {
        this.spawnedEntities.remove(entity.getUniqueId());
        entity.remove();
    }

    public void removeEntity(UUID uuid)
    {
        Entity entity = getPlugin().getServer().getEntity(uuid);
        this.spawnedEntities.remove(uuid);
        if (entity != null) entity.remove();
    }

    public boolean isSpawned(Entity entity)
    {
        return this.spawnedEntities.contains(entity.getUniqueId());
    }

    public void removeDead()
    {
        for (UUID id : spawnedEntities)
        {
            Entity entity = getPlugin().getServer().getEntity(id);
            if (entity == null || entity.isDead())
            {
                removeEntity(id);
            }
        }
    }

    public void clearEntities()
    {
        for (UUID id : spawnedEntities)
        {
            Entity entity = getPlugin().getServer().getEntity(id);
            if (entity != null)
            {
                entity.remove();
            }
        }

        this.spawnedEntities.clear();
    }

    public Keizaal getPlugin()
    {
        return this.plugin;
    }
}
